package main

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

var set []int32

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	if !scanner.Scan() {
		os.Stderr.Write([]byte("fail to scan first argument"))

		return
	}

	lengthBytes := scanner.Bytes()
	length, err := strconv.Atoi(string(lengthBytes))
	if err != nil {
		os.Stderr.Write([]byte("Fail to parse lencth"))

		return
	}

	if length == 0 {
		os.Stderr.Write([]byte("Length is not defined"))

		return
	}

	if !scanner.Scan() {
		os.Stderr.Write([]byte("Fail to scan second arg"))

		return
	}

	setBytes := scanner.Bytes()
	strInts := strings.Split(string(setBytes), " ")
	set, err = toArrInt(strInts)
	if err != nil {
		os.Stderr.Write([]byte("fail to convert []string to []int32"))

		return
	}

	resultCounter := getCount(set)
	res := strconv.Itoa(resultCounter)
	os.Stdout.Write([]byte(res))
}

func toArrInt(in []string) ([]int32, error) {
	res := make([]int32, 0, len(in))
	for i := 0; i < len(in); i++ {
		integer, err := strconv.Atoi(in[i])
		if err != nil {
			return nil, err
		}

		res = append(res, int32(integer))
	}

	return res, nil
}

func getMaxElementIdex(in []int32) int {
	var max int
	for i := 0; i < len(in); i++ {
		if in[i] >= in[max] {
			max = i
		}
	}

	return max
}

func getMinElementIdex(in []int32, maxIndex int) int {
	min := maxIndex
	for i := maxIndex; i > -1; i-- {
		if in[i] <= in[min] {
			min = i
		}
	}

	return min
}

func getCount(in []int32) int {
	resultCounter := 0
	if len(in) == 0 {
		return resultCounter
	}
	resultCounter++
	max := getMaxElementIdex(in)
	right := in[max+1:]

	min := getMinElementIdex(in, max)
	left := in[:min]

	resultCounter = resultCounter + getCount(left) + getCount(right)

	return resultCounter
}
